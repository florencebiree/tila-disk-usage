#!/bin/env python3
# -*- coding: utf-8 -*-
###############################################################################
#       tiladu.py
#       
#       Copyright © 2023, Florence Birée <florence@biree.name>
#       
#       This file is a part of tila-disk-usage.
#       
#       This program is free software: you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation, either version 3 of the License, or
#       (at your option) any later version.
#       
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       
#       You should have received a copy of the GNU General Public License
#       along with this program.  If not, see <http://www.gnu.org/licenses/>.
#       
###############################################################################
"""Make reports for Tila.im users disk usage"""

__author__ = "Florence Birée"
__version__ = "0.1"
__license__ = "AGPL"
__copyright__ = "Copyright © 2023, Florence Birée <florence@biree.name>"
__revision__ = "$Revision: $"
__date__ = "$Date: $"

import os
import sys
import shutil
import subprocess
import humanize
import argparse
import configparser
import requests
import smtplib
from email.message import EmailMessage
from statistics import mean, median
from seafile_webapi import SeafileClient

#### utility functions ####

def hmsize(size):
    """Return an humanized representation of a size in KiB
    
    hardcoded: french translation B -> o (not managed by humanize)
    """
    if size:
        return humanize.naturalsize(size*1024, binary=True).replace('B', 'o')
    else:
        return None

#### generic classes ####

class DUUser:
    """Generic class with disk usage for one user in a given DUSource
        
        Available data:
            user_email: e-mail to identify the user
            username: username if any
            disk_usage: disk usage in KiB
            du_url: URL where the user can see it's disk usage
    
    """
        
    def __init__(self, user_email):
        """Create a new DUUser, identified by it's e-mail
        
            disk-usage is computed at the DUUser creation
        """
        self.user_email = user_email
        self.username = None
        self.disk_usage = None
        self.du_url = None
        
    def du(self, path):
        """Return the disk usage of a path in KiB"""
        return int(subprocess.check_output(
            ['du','-s', path]
        ).split()[0].decode('utf-8'))        

class DUSource:
    """Generic disk-usage source
    
        Available data:
            total_used: total disk space used, KiB
            system_users: usernames of system_users
            free_space: free space in KiB, from the filesystem
            ordered_users: DUUser ordered from less space to most space used
            average_du: average computed disk usage in KiB
            median_du: median computed disk usage in KiB
            comment: a user-friendly text about disk space in this source
    
    """
    # if strictly under this size, don't count an user
    MIN_THRESHOLD = 0
    NAME = "Generic source"
    
    UserData = DUUser
    
    def __init__(self):
        """Create a new DUSource"""
        self.total = None
        self.total_used = None
        self.system_users = []
        self.free_space = None
        self.ordered_users = []
        self.average_du = None
        self.median_du = None
        self.comment = None
    
    def df(self, path):
        """Return a tuple of (total, used, free) disk space for `path`
        
        All size in KiB
        """
        kib = lambda s:s/1024
        total, used, free = shutil.disk_usage(path)
        return (kib(total), kib(used), kib(free))
    
    def compute(self):
        """Compute the disk usage"""
        raise NotImplemented
        
    def _order(self):
        """Order the disk_usage list"""
        self.ordered_users.sort(key=lambda ud: ud.disk_usage)
    
    def _make_stats(self):
        """Make stats from self.ordered_users"""
        if self.ordered_users:
            # compute average_du
            self.average_du = mean(ud.disk_usage for ud in self.ordered_users) 
            
            # compute median_du
            self.median_du = median(ud.disk_usage for ud in self.ordered_users)


#### Mailboxes ####

class MailBoxUser(DUUser):
    """The mailbox used space for an user"""
    
    def __init__(self, user_email, username, path_pattern, du_url=None):
        super().__init__(user_email)
        self.username = username
        self.du_url = du_url
        self.path_pattern = path_pattern
        
        # get size
        self.disk_usage = self.du(
            self.path_pattern.format(username=self.username)
        )
        

class MailBoxSource(DUSource):
    """Compute mailboxes sizes"""

    NAME = "Boîtes mail"
    MIN_THRESHOLD = 1024 # 1 MiB

    UserData = MailBoxUser
    
    def __init__(self, mail_domain='localhost', mailbox_root='/home', path_pattern='/home/{username}/Maildir', du_url=None):
        """Create a new MailBoxSource"""
        super().__init__()
        self.mail_domain = mail_domain
        self.mailbox_root = mailbox_root
        self.path_pattern = path_pattern
        try:
            (total, used, free) = self.df(self.mailbox_root)
        except FileNotFoundError:
            (total, used, free) = (None, None, None)
        self.disk_size = total
        self.total_used = used
        self.free_space = free
        self.du_url = du_url
        self.comment = """
Un ordre de grandeur concernant les e-mails : si tu veux faire du tri, cherche
les mails au dessus de 1Mio (les plus petits, ceux comptés en Kio, ne prennent
pas beauoup de place).
Dans le webmail https://webmail.tila.im/ puis dans le menu au-dessus des
dossiers, tu peux demander à voir la taille des dossiers, pour trouver ceux
où il y a les mails les plus lourds. Puis, dans les options au-dessus des mails,
tu peux demander dans "colonne de tri" de trier les mails par taille.
"""
        
    def compute(self):
        """Compute disk usage for all users""" 
        # check if the root exists, else abort the computation
        if not os.path.isdir(self.mailbox_root):
            return
        
        # get user list from filesystem
        user_list = []
        for dirname in os.listdir(self.mailbox_root):
            # keep only dirname wich are actual dir and not symlinks
            fullpath = os.path.join(self.mailbox_root, dirname)
            if os.path.isdir(fullpath) and not os.path.islink(fullpath):
                # Check if the user have a Mailbox
                if os.path.isdir(self.path_pattern.format(username=dirname)):
                    # do not append system users
                    if not dirname in self.system_users:
                        user_list.append(dirname)  
        
        # compute disk usage
        for u_id in user_list:
            ud = self.UserData('{}@{}'.format(u_id, self.mail_domain), u_id, self.path_pattern)
            if ud.disk_usage > self.MIN_THRESHOLD:
                self.ordered_users.append(ud)
        
        # order the list, compute stats
        self._order()
        self._make_stats()

#### Websites ####

class WWWUser(MailBoxUser):
    """The website used space for an user"""

    def __init__(self, user_email, username, path_pattern):
        super().__init__(user_email, username, path_pattern)
        self.du_url = None

class WWWSource(MailBoxSource):
    """Compute websites sizes"""

    NAME = "Sites web (hors wordpress mutualisé)"
    MIN_THRESHOLD = 1024 # 1 MiB

    UserData = WWWUser
    
    def __init__(self, mail_domain='localhost', www_root='/var/www/', path_pattern='/var/www/{username}', system_users=None):
        """Create a new WWWSource"""
        super().__init__(mail_domain, www_root, path_pattern)
        self.comment = None
        self.system_users = system_users if system_users else []

#### Seafile ####

class SeafileUser(DUUser):
    """The seafile used space for an user"""
    
    def __init__(self, user_email, user_conv_email, disk_usage):
        super().__init__(user_conv_email)
        self.user_seaf_email = user_email
        self.disk_usage = disk_usage / 1024 # disk usage is in B in Seafile, we want KiB

class SeafileSource(DUSource):
    """Compute seafile sizes"""
    
    NAME = "Données dans Seafile"""
    MIN_THRESHOLD = 1024 # 1 MiB
    
    UserData = SeafileUser
    
    def __init__(self, seaf_data, seaf_url, seaf_admin_email, seaf_admin_token, mail_conv_table=None):
        """Create a new SeafileSource"""
        super().__init__()
        self.seaf_data = seaf_data
        self.seaf_url = seaf_url
        self.seaf_admin_email = seaf_admin_email
        self.seaf_admin_token = seaf_admin_token
        self.mail_conv_table = mail_conv_table if mail_conv_table else {}
        try:
            (total, used, free) = self.df(self.seaf_data)
        except FileNotFoundError:
            (total, used, free) = (None, None, None)
        self.disk_size = total
        self.total_used = used
        self.free_space = free
        self.comment = """
Petit rappel pour Seafile : par défaut, les bibliothèques ont un historique
d'activé. Dans ce cas, un fichier supprimé ne l'est pas réellement sur le
disque, puisqu'il reste dans l'historique. Sa place n'est donc pas récupéré.
On peut régler, bibliothèque par bibliothèque, les paramètres d'historique,
pour le garder à vie (par défaut), pendant un certain temps, ou jamais.
"""
        
    def compute(self):
        """Compute disk usage for all users""" 
        # connect to the API
        client = SeafileClient(self.seaf_url, v12auth=True)
        client.authenticate(self.seaf_admin_email, token=self.seaf_admin_token)
        
        # get user list from API
        user_list = client.list_users(1, 1000) # 1000 user per page
                    # TODO : this can limit the results…
        
        # for each user:
        for seaf_user in user_list['data']:
            # convert its email
            user_email = seaf_user['contact_email']
            username, domain = user_email.split('@')
            if domain in self.mail_conv_table:
                conv_email = '{}@{}'.format(username, self.mail_conv_table[domain])
            else:
                conv_email = user_email
            # get disk usage
            du = seaf_user['quota_usage']
            if du > self.MIN_THRESHOLD * 1024:
                self.ordered_users.append(self.UserData(user_email, conv_email, du))
        
        # order the list, compute stats
        self._order()
        self._make_stats()


#### Wordpress ####

class WordpressUser(DUUser):
    """The wordpress used space for an user (wordpress network install)"""
    
    def __init__(self, user_email, user_conv_email, disk_usage):
        super().__init__(user_conv_email)
        self.user_wordpress_email = user_email
        self.disk_usage = disk_usage

class WordpressSource(DUSource):
    """Compute wordpress sizes"""
    
    NAME = "Données sur l'installation mutualisée de Wordpress"""
    MIN_THRESHOLD = 0
    #1024 # 1 MiB
    
    UserData = WordpressUser
    
    def __init__(self, wp_root, wp_url, wp_admin_email, wp_app_password, mail_conv_table=None):
        """Create a new SeafileSource"""
        super().__init__()
        self.wp_root = wp_root
        self.wp_url = wp_url
        self.wp_admin_email = wp_admin_email
        self.wp_app_password = wp_app_password
        self.mail_conv_table = mail_conv_table if mail_conv_table else {}
        try:
            (total, used, free) = self.df(self.wp_root)
        except FileNotFoundError:
            (total, used, free) = (None, None, None)
        self.disk_size = total
        self.total_used = used
        self.free_space = free
        
    def compute(self):
        """Compute disk usage for all users""" 
        # get the site list
        site_data = requests.get(
            '{}/wp-json/wp/v2/sites/'.format(self.wp_url),
            auth=(self.wp_admin_email, self.wp_app_password)
        ).json()
        
        wp_users = {}
        for site in site_data:
            # build the url
            site['url'] = 'https://{}{}'.format(site['domain'], site['path'])
            # compute the disk usage (not for site #0)
            if site['blog_id'] != "1":
                content_path = os.path.join(self.wp_root, 'wp-content/uploads/sites', site['blog_id'])
                try:
                    site['disk_usage'] = int(subprocess.check_output(
                        ['du','-s', content_path]
                    ).split()[0].decode('utf-8'))
                except subprocess.CalledProcessError:
                    site['disk_usage'] = None  
            else:
                site['disk_usage'] = None
            # get email admin of this site
            try:
                users_data = requests.get(
                    '{}/wp-json/wp/v2/users?context=edit'.format(site['url']),                                
                    auth=(self.wp_admin_email, self.wp_app_password)                     
                ).json()
            except requests.exceptions.JSONDecodeError:
                print("Impossible de récupérer le mail d'admin WP de {}".format(site['url']), file=sys.stderr)
                users_data = []
            for user in users_data:
                user_email = user['email']
                if user_email in wp_users:
                    wp_users[user_email].append(site)
                else:
                    wp_users[user_email] = [site]
        
        # for each user: add the data of each sites
        for user_email in wp_users:
            # convert its email
            username, domain = user_email.split('@')
            if domain in self.mail_conv_table:
                conv_email = '{}@{}'.format(username, self.mail_conv_table[domain])
            else:
                conv_email = user_email
            
            # add all user data for wordpress
            du = sum(site['disk_usage'] for site in wp_users[user_email] if site['disk_usage'])
            
            if du > self.MIN_THRESHOLD * 1024:
                self.ordered_users.append(self.UserData(user_email, conv_email, du))
            
        # order the list, compute stats
        self._order()
        self._make_stats()


#### Reports ####

class PerServiceReport:
    """Produce a per services report"""
    
    def __init__(self, source_list):
        """Create a new report, with the list of DUSource to use"""
        self.source_list = source_list
    
    def report(self):
        """Compute the disk usage, and return the report"""
        report = ""
        for src in self.source_list:
            src.compute()
            report += src.NAME + '\n'
            for i, u in enumerate(src.ordered_users):
                report += '{}\t{}: {}\n'.format(i, u.user_email, hmsize(u.disk_usage))
            report += '\n'
            report += 'Moyenne : {}, médiane : {}\n'.format(hmsize(src.average_du), hmsize(src.median_du))
            report += '\n'
            report += ('-' * 30) + '\n'
        return report

class PerUserReport(PerServiceReport):
    """Produce a per user report"""
    
    def __init__(self, source_list):
        """Create a new report, with the list of DUSource to use"""
        super().__init__(source_list)
        self.user_reports = {}
    
    def _search(self, source, user_email):
        """Return (nth, du_user) from a source and a user_email
        
        nth is the len - index of the user_email in ordered_users
        
        return (None, None) if not found
        """
        for i, u in enumerate(source.ordered_users):
            if u.user_email == user_email:
                return ((len(source.ordered_users) - i), u)
        
        return (None, None)
    
    def report(self):
        """Compute the disk usage, and return the report
        
        also, store a per-user report in self.user_reports dict
        """
        data_sources = []
        # compute disk usage for each sources
        for src in self.source_list:
            src.compute()
            data_sources.append(src)
        
        # make the global list of users
        glob_users = set()
        for dsrc in data_sources:
            glob_users.update(user.user_email for user in dsrc.ordered_users)
        
        # make a report for each user
        report = ""
        for user_email in glob_users:
            ureport = "Rapport pour {} ================\n".format(user_email)
            ureport += "\n"
            for src in data_sources:
                nth, ud = self._search(src, user_email)
                if ud:
                    ureport += "{}:\n".format(src.NAME)
                    ureport += "Espace utilisé : {} ({}eme plus grand·e utilisateurice du serveur sur {} utilisateurices)\n".format(
                        hmsize(ud.disk_usage), nth, len(src.ordered_users)
                    )
                    ureport += "C'est {:3.2f}% de l'espace disque utilisé, et {:3.2f}% de l'espace disque total.\n".format(
                        ud.disk_usage / src.total_used * 100,
                        ud.disk_usage / src.disk_size * 100
                    )
                    ureport += "Taille totale du disque : {}, espace utilisé : {} ({:3.2f}%), espace libre : {} ({:3.2f}%)\n".format(
                        hmsize(src.disk_size),
                        hmsize(src.total_used),
                        src.total_used / src.disk_size * 100,
                        hmsize(src.free_space),
                        src.free_space / src.disk_size * 100
                    )
                    ureport += '(Moyenne des utilisateurices : {}, médiane des utilisateurices : {})\n'.format(hmsize(src.average_du), hmsize(src.median_du))
                    if ud.du_url:
                        ureport += ud.du_url + '\n'
                    if src.comment:
                        ureport += src.comment
                    ureport += '\n'
            self.user_reports[user_email] = ureport
            report += ureport
        return report

class EMailReport(PerUserReport):
    """Produce a per user report"""
    
    def __init__(self, source_list, from_mail="root@tila.im"):
        """Create a new report, with the list of DUSource to use"""
        super().__init__(source_list)
        self.from_mail = from_mail
        self.subject = "[Tila.im] Rapport d'utilisation de l'espace disque"
        self.intro = """Salut à toi, utilisateurice de Tila.im,

Ce petit mail est automatique, il est envoyé chaque trimestre. Son but, c'est
de te donner quelques repères sur ton utilisation d'espace disque de Tila.im.
L'idée générale, c'est que quand les disques sont pleins, on peut se cotiser
pour en racheter, et que oui, certaines personnes ont un usage plus important
que d'autres, et c'est pas grave.

Mais des disques durs, c'est quand même des trucs électroniques qui ont un
impact humain et écologique non négligeable à leur fabrication, alors
utilisons-les en conscience :)

Enfin, petit rappel sur les disques durs du serveur :
https://thefool.tila.im/doc/matos/
et la gestion des sauvegardes :
https://tila.im/doc/sauvegardes.html
-> un Go do données compte double sur le serveur, car chaque disque est en
miroir avec un autre, pour éviter les pannes de disques durs. De plus, les
données sont sauvegardées toutes les nuits sur un troisième disque dur distant.
Donc chaque Go de données compte triple !
"""
        self.outro = """

Pour finir, recevoir ce mail ne veut pas forcément dire que tu utilises trop
d'espace, et le but c'est que cet espace corresponde à tes besoins.

Si tu te pose des questions sur comment gérer tout ça, que tu ne comprends pas
tout, n'hésite pas à écrire à Florence qui gère le serveur :
        florence@biree.name
"""
    
    def send_emails(self):
        """For each user, send an email with the report"""
        for user in self.user_reports:
            msg = EmailMessage()
            msg.set_content(
                self.intro
                + self.user_reports[user]
                + self.outro
            )
            msg['Subject'] = self.subject
            msg['From'] = self.from_mail
            msg['To'] = user
            
            # Send the message via our own SMTP server.
            s = smtplib.SMTP('localhost')
            try:
                s.send_message(msg)
            except smtplib.SMTPRecipientsRefused:
                sys.stderr.write("Erreur d'envoi à {}.\n".format(user))
            s.quit()

#### Main program ####

if __name__ == '__main__':
    # parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--user", help="build a per user report (instead of per services)",
                        action="store_true")
    parser.add_argument("-m", "--email", help="send a per user report by mail",
                        action="store_true")
    parser.add_argument("-c", "--config", help="path of the configuration file")
    args = parser.parse_args()
    
    # load the configuration
    config = configparser.ConfigParser()
    user_config_filename = os.path.expanduser('~/.config/tiladu.ini')
    etc_config_filename = '/etc/tiladu.ini' 
    if args.config and os.path.isfile(args.config):
        config.read(args.config)
    elif os.path.isfile(user_config_filename):
        config.read(user_config_filename)
    elif os.path.isfile(etc_config_filename):
        config.read(etc_config_filename)
    else:
        print("No config file found, either by -c, in {} or in {}.".format(user_config_filename, etc_config_filename), file=sys.stderr)
        sys.exit(1)
    
    source_list = []
    
    for src in config.sections():
        if src.lower() == 'mailbox':
            source_list.append(MailBoxSource(**config[src]))
        elif src.lower() == 'www':
            src_args = dict(config[src])
            if 'system_users' in src_args:
                su = src_args['system_users']
                src_args['system_users'] = su.replace(' ', '').split(',')
            source_list.append(WWWSource(**src_args))
        elif src.lower() == 'seafile':
            src_args = dict(config[src])
            if 'mail_conv_table' in src_args:
                mct = src_args['mail_conv_table']
                mct_dict = {}
                for conv in mct.replace(' ', '').split(','):
                    k, v = conv.split(':')
                    mct_dict[k] = v
                src_args['mail_conv_table'] = mct_dict
            source_list.append(SeafileSource(**src_args))
        elif src.lower() == 'wordpress':
            src_args = dict(config[src])
            if 'mail_conv_table' in src_args:
                mct = src_args['mail_conv_table']
                mct_dict = {}
                for conv in mct.replace(' ', '').split(','):
                    k, v = conv.split(':')
                    mct_dict[k] = v
                src_args['mail_conv_table'] = mct_dict
            source_list.append(WordpressSource(**src_args))
        
    
    # produce reports
    if args.user:
        # per user report
        report = PerUserReport(source_list)
        print(report.report())
    elif args.email:
        # send mail
        report = EMailReport(source_list)
        report.report()
        report.send_emails()
    else:
        # per services report
        report = PerServiceReport(source_list)
        print(report.report())
    
    
