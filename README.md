# tila-disk-usage

This program compute a disk usage report for each user of a server
(created for [Tila.im](https://tila.im/).

Disk usage is reported for this server components:

- mailboxes
- [Seafile](https://seafile.com/) usage
- web sites usage
- wordpress usage

Dependencies:

  - python3-humanize
  - [seafile_webapi](https://framagit.org/florencebiree/python-seafile-webapi)

For the wordpress source, we use the API provided by this wordpress extension:
https://github.com/brettkrueger/multisite-rest-api/
